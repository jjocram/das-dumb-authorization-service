from flask import Flask, request
from flask_cors import CORS
from web3 import Web3

from config import *

app = Flask(__name__)
CORS(app, support_credentials=True)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
database = {}


@app.route("/provide", methods=['POST'])
def provide():
    """
    data: {
        data_owner_id,
        encrypted_password
    }
    :return: data
    """
    data = request.get_json()
    database[data["data_owner_id"]] = data["encrypted_password"]
    return data


@app.route("/request-access", methods=['POST'])
def request_access():
    """
    data: {
        data_consumer_address
        data_owner_id
    }
    :return: JSON with password if data_consumer_address is allowed to access
    """
    data = request.get_json()
    web3_client = Web3(Web3.HTTPProvider(NETWORK_ADDRESS))
    if not web3_client.isConnected():
        print(f"Cannot connect to the ethereum network at: {NETWORK_ADDRESS}")
        return {
            "error": f"Cannot connect to the ethereum network at: {NETWORK_ADDRESS}"
        }

    print(data)

    sspermission_contract = web3_client.eth.contract(address=Web3.toChecksumAddress(SSPERMISSIONE_ADDRESS),
                                                     abi=SSPERMISSIONE_ABI)
    permission = sspermission_contract.functions.checkPermissions(Web3.toChecksumAddress(data["data_consumer_address"]),
                                                                  data["data_owner_id"]).call()

    return {
        "error": None,
        "allowed": permission,
        "password": database.get(data["data_owner_id"], "Not set by the user") if permission else None
    }

print(f"Config:\n\tSSPermission contract address: {SSPERMISSIONE_ADDRESS}")

app.run(host="127.0.0.1",
        port=3001)
