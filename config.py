from json import load
from os import environ


NETWORK_ADDRESS = "http://localhost:8545"

SSPERMISSIONE_ADDRESS = environ.get("SSPERMISSION_ADDRESS")
SSPERMISSIONE_ABI = load(open("SSPermission.json"))
